provider "aws" { 
  region = var.aws_region 
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}
resource "aws_vpc" "technoconfig" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
}
resource "aws_internet_gateway" "technoconfigIGW" {
  vpc_id = aws_vpc.technoconfig.id
}
resource "aws_subnet" "eu-north-1a-public1" {
  cidr_block = var.public_subnet_cidr
  vpc_id = aws_vpc.technoconfig.id
  availability_zone = "eu-north-1a"
  tags = {
    Name = "Public subnet"
  }
}
resource "aws_route_table" "eu-north-1a-public1" {
  vpc_id = aws_vpc.technoconfig.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.technoconfigIGW.id
  }
  tags = {
    Name = "RT-public"
  }
}
resource "aws_route_table_association" "eu-north-1a-public1" {
  subnet_id = aws_subnet.eu-north-1a-public1.id
  route_table_id = aws_route_table.eu-north-1a-public1.id
}
resource "aws_instance" "centos" {
  count = var.instance_count
  ami = "ami-0358414bac2039369"
  instance_type = "t3.micro"
  key_name = "aws_ssh2"
  vpc_security_group_ids = ["${aws_security_group.web.id}"]
  subnet_id = "${aws_subnet.eu-north-1a-public1.id}"
  associate_public_ip_address = true
  source_dest_check = false
  tags = {
    Name = "${element(var.instance_tags, count.index)}"
  }
}

resource "aws_security_group" "web" {
  name = "SG_web"
  description = "Allow all http connections"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8082
    to_port = 8082
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 3000
    to_port = 3000
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8081
    to_port = 8081
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8088
    to_port = 8088
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  } 
  ingress {
    from_port = 2003
    to_port = 2003
   protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 2004
    to_port = 2004
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 2023
    to_port = 2023
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 2024
    to_port = 2024
   protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8125
    to_port = 8125
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8126
    to_port = 8126
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.technoconfig.id
}
output "Grafana" {
  value = aws_instance.centos.0.public_ip
}
output "Postgres" {
  value = aws_instance.centos.1.public_ip
}
output "Tomcat" {
  value = aws_instance.centos.2.public_ip
}
